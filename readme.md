## Question #1 (maintenance).

What is wrong with the above code?

It did not properly calculate negative values.

If you find it wrong, please fix the code above without using the "*" or "/" operator or Absolute call?

Done with the code provided.

As part of our development process we test all methods at a code level. Which input values would you use to do the testing?

Negative values is enough for both values and each of the values.

For later discussion: What else worries you as you fix this problem?

Large numbers.

## Question #2 (SQL)

What would be the output of the following select statements?

1. Select USA.NAME, EU.NAME From USA, EU Where USA.ID = EU.ID

    name    name
1.  Thomas  Thomas

2. Select USA.NAME, EU.NAME From USA left join EU on (USA.ID = EU.ID)

    name    name
1.  Thomas  Thomas
2.  Cindy	[NULL]

3. Select USA.NAME, EU.NAME From USA, EU

	name	name
1.	Thomas	Francois
2.	Thomas	Thomas
3.	Cindy	Francois
4.	Cindy	Thomas

For later discussion: we use those tables to keep track of our European and American customers.
Please provide a critique to that table design (is it good? How could it be better?).

It needs to have a unique ID for each table, so it didn't collide with the same ID but different names.

## Question #3 (algorithm)

The following class describes a node on a binary tree. A node can have a left child, or a right child, or both, or no child at all.

QUESTION:

Write the content of the method below that counts the maximum number of levels in a given tree. Please notice that this is NOT counting the TOTAL number of nodes, but counting the DEPTH.

The answer is in the app3.py file.

## Question #4 (performance)

The following method will find the intersection (duplicates) of two given sets.

QUESTIONS:

What would be the effect on performance in these two cases:

Do you have any recommendations to improve the performance?  Feel free to change the above method.

The answer is in the app4.py file.

## Question #5 (algorithm)

A Prime number is defined as a positive integer greater than 1 that is divisible only by 1 and itself. When the number is divided by any other positive integers, it would have remainders.

QUESTION:

Please write a method to print out ALL the Prime numbers between 2 and 100.

The answer is in the app5.py file.
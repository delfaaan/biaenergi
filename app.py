def multiply(x, y):
	if x < 0:
		x = negate(x)
		y = negate(y)

	total = 0

	while x > 0:
		if x & 1:
			total = add(total, y)
		x = halve(x)
		y = double(y)

	return total

def negate(num):
	return add(~num, 1)

def add(a, b):
	while b != 0:
		carry = a & b
		a = a ^ b
		b = carry << 1
	return a

def halve(num):
	return num >> 1

def double(num):
	return num << 1

# print(multiply(5, 3))
# print(multiply(-5, 3))
# print(multiply(5, -3))
# print(multiply(-5, -3))

print(multiply(0, 3))
print(multiply(-1, 3))
print(multiply(1, 3))
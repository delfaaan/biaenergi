def intersect(bagA, bagB):
	setA = set(bagA)
	setB = set(bagB)
	result = list(setA.intersection(setB))

	return result

# Test cases:
bagA_large = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20]
bagB_small = [2, 5, 10]

bagA_small = [5, 10]
bagB_large = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20]

bagA_non_overlapping = [1, 3, 5, 7, 9]
bagB_non_overlapping = [2, 4, 6, 8, 10]

bagA_empty = []
bagB_empty = []

bagA_common = [1, 2, 3, 4, 5]
bagB_common = [3, 4, 5, 6, 7]

bagA_duplicates = [1, 2, 3, 4, 4, 5, 5, 5]
bagB_duplicates = [4, 4, 5, 6, 7]

bagA_negative = [1, 2, 3, -4, -5]
bagB_negative = [-4, -5, -6, -7]

bagA_mixed = [1, 'apple', True, 3.14, None]
bagB_mixed = [3.14, True, 'banana', 42]

bagA_large_overlap = list(range(1, 1001, 2))
bagB_large_overlap = list(range(500, 1501, 2))

bagA_large_no_overlap = list(range(1, 1001, 2))
bagB_large_no_overlap = list(range(2, 1002, 2))

print(intersect(bagA_large, bagB_small))
print(intersect(bagA_small, bagB_large))
# print(intersect(bagA_non_overlapping, bagB_non_overlapping))
# print(intersect(bagA_empty, bagB_empty))
# print(intersect(bagA_common, bagB_common))
# print(intersect(bagA_duplicates, bagB_duplicates))
# print(intersect(bagA_negative, bagB_negative))
# print(intersect(bagA_mixed, bagB_mixed))
# print(intersect(bagA_large_overlap, bagB_large_overlap))
# print(intersect(bagA_large_no_overlap, bagB_large_no_overlap))
def calculateMaxDepth(n):
	if n is None:
		return 0

	left_depth = calculateMaxDepth(n.left)
	right_depth = calculateMaxDepth(n.right)

	return max(left_depth, right_depth) + 1